//Collections
var Rooms = new Meteor.Collection("rooms");
var Rolls = new Meteor.Collection("rolls");

//Publish
Meteor.publish("rooms", function() {
	return Rooms.find();
});
Meteor.publish("rollsByRoomId", function(roomId) {
	return Rolls.find({"roomId": roomId});
});

Meteor.methods({
	getRoom : function(roomId) {
		
		return Rooms.findOne({_id:roomId});
	}

});