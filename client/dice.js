//TODO
//wait for collections to load http://stackoverflow.com/questions/10099968/meteor-using-collection-on-client-startup
//reactive subscription based on roomId
//create chatter application

//main
Template.main.loading = function() {
	return !roomsHandle && !roomsHandle.ready();
};
Template.main.page = function() {
	var pageId = Session.get("pageId");
	var template = Template[pageId];
	if (!template) return Router.errorPage('This page doesn\'t exist.');
	return template();
};

//welcomePage
Template.welcomePage.events({
	"click #create-new-room": function() {
		var newRoomId = Rooms.insert({});
		Router.navigate("/room/"+newRoomId, true);
	},
	"click #join-existing-room": function() {
		var existingRoomId = $("#input-room-id").val();
		if (!existingRoomId) return;

		Router.navigate("/room/"+existingRoomId, true);
	}
});

//roomPage
Template.roomPage.roomId = function() {
	return Session.get("roomId");
};

Template.roomPage.lastRoll = function() {
	return Rolls.findOne({}, {sort: {timestamp: -1}});
};

Template.roomPage.rolls = function() {
	return Rolls.find({}, {sort: {timestamp: -1}});
};

Template.roomPage.events({
	"click #roll": function() {
		var roll = Math.ceil(Math.random() * 6);
		var room = Session.get("roomId");
		var time = new Date().getTime();
		Rolls.insert({value: roll, roomId: room, timestamp: time});
	}
});

//errorPage
Template.errorPage.message = function() {
	return Session.get("errorPage.message");
};

//ROUTES
var DiceRouter = Backbone.Router.extend({
	routes: {
		"" : "defaultPage",
		"room/:roomId" : "roomPage"
	},
	defaultPage: function() {
		Session.set("pageId", "welcomePage");
	},
	roomPage: function(roomId) {
		var that = this;
		//--- this will always evaluate to false when comming opening this url in browser (Rooms collection is not fetched to the client), how to wait until roomsHandle.ready() ---???
		Meteor.apply("getRoom", [roomId], function(error, room) {
			if (!room) {
				that.errorPage("Room id " + roomId + " doesn't exist.");
				return;
			}

			Session.set("roomId", roomId);
			Session.set("pageId", "roomPage");	
		});
		
	},
	errorPage: function(message) {
		Session.set("errorPage.message", message);
		Session.set("pageId", "errorPage");
	}
});

var Router = new DiceRouter();

//STARTUP
Meteor.startup(function() {
	Backbone.history.start({pushState: true});
});

//Collections
var Rooms = new Meteor.Collection("rooms");
var Rolls = new Meteor.Collection("rolls");

//Subscribe
var roomsHandle = Meteor.subscribe("rooms");
Deps.autorun(function() {
	var rollsHandle = Meteor.subscribe("rollsByRoomId", Session.get("roomId"));
});